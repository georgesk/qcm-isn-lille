# Extrait du courriel de Regis LECLERCQ : NSI - QCM E3C #

`Message-id: <9da81a8c-5f50-2663-69b1-2c373fbac348@ac-lille.fr>`

[...] des QCM pour les élèves qui ne
poursuivent pas la spécialité NSI à la fin de la classe de première, nous
sommes amenés *à vous solliciter pour créer une question* dans chacun des 7
thèmes du programme de Première : 

  *  types et valeurs de base ; 
  *  types construits ;
  *  traitement de données en tables ;
  *  interactions entre l’Homme et la machine sur le Web ;
  *  architectures matérielles et systèmes d’exploitation ;
  *  langages et programmation ;
  *  algorithmique.
  
Il est prévu qu'une réponse juste à une question rapporte 3 points, une
réponse fausse coûte 1 point, une absence de réponse ne rapporte aucun point.

Il s’agit de poser une question et 4 réponses possibles.

La formulation des 4 réponses doit être à peu près similaire, de sorte de ne
pas particulariser l’une ou l’autre : il ne faudrait pas qu’on puisse repérer
la réponse juste simplement parce qu’elle est formulée différemment. On
n’utilisera pas de réponse du type « aucune des trois réponses précédentes ».

L’objectif n’est pas de chercher à induire le candidat en erreur, mais à
certifier un niveau de maîtrise.

