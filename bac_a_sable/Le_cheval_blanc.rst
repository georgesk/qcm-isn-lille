
==========================
Le cheval blanc d'Henri IV
==========================

Quelle est la couleur du cheval Blanc d'Henri IV ?

Cette question a été débattue sur `plus d'un site web <https://www.qwant.com/?q=cheval+blanc+henri+iv>`__.

|tableau|

  1. Blanc
  2. Vert
  3. Crème
  4. Sale
  
..  "OK: 2"
.. |tableau| image:: https://i0.wp.com/passionchateau.fr/wp-content/uploads/2015/01/77-5-Fraise-Mauzaisse-Henri-IV.jpg?resize=422%2C567
