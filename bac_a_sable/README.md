# But du Bac à sable #

Ici, c'est la zone de tous les essais foireux.

L'idée, c'est d'y déposer des modèles de QCM inintéressants, juste pour
en tester le fonctionnement formel.

Comme il s'agit d'un travail collaboratif, les QCM sources seront
de préférence à un format ouvert et de très bas niveau : format
texte. Afin d'améliorer un peu ça, on peut admettre des formats plus
sophistiqués, comme le `MarkDown` (.md) ou le `ReSTructured text` (.rst)
parce que ça ressemble vraiement à du texte bien lisible, et qu'on dispose
de bonnes bibliothèques de programmes qui savent les interpréter.

## Syntaxe proposée pour une question du QCM ##

  * le titre, le préambule et le corps de la question, tous les
    formats permis par `MarkDown` ou `ReSTructured text` sont permis,
	ce qui autorise des tableaux, des formules (sous LaTeX), des liens,
	etc. ;
  * Une liste numérotée de quatre items, qui sont les réponses au choix ;
  * Un commentaire contenant une phrase identifiant clairement la bonne réponse.
	
## Fichiers sources ##

Les fichiers avec les extensions `.md` et `.rst` de ce dossier sont
considérés comme fichiers sources. Un programme au moins permet de vérifier
si le contenu de ces fichiers est compréhensible comme un QCM.

## Scripts ##

Des programmes, par exemple en Python3 (extension `.py`) peuvent servir,
pour :
  *  Vérifier la syntaxe d'un QCM au format  `.md` ou `.rst`
  *  Traduire un fichier source en formats acceptables par d'autres
     systèmes, comme Pronote, Moodle, Hot Potatoes, etc.
  *  Traduire des QCM issus d'autres systèmes vers un format `.md` ou `.rst`
  
